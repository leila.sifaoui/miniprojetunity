﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour

{
    AudioSource myAudio;
    public GameObject ResultScreen;
    // Start is called before the first frame update
    void Start()
    {
        myAudio = GetComponent<AudioSource>();
        myAudio.PlayDelayed(4.0f);

        Invoke("Results", myAudio.clip.length); //clip.length gives back the audio length with seconds, once the audio is finished the method gonna get invoked
    }

    void Results()
    {
        Debug.Log("Song Finished");
        GameManager.Instance.ResultScreen();
        ResultScreen.SetActive(true);
    }

}
