﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatsMove : MonoBehaviour
{


    bool CanBePressed;



    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {


        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 15 * Time.deltaTime);

        if (Input.touchCount != 0) //if the screen is touched
        {
            if (CanBePressed) //if the Beat is in the trigger area
            {

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); //ray will get the position of the touch
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    float h = Mathf.Round(hit.transform.position.x * 100f) / 100f; //Round gives arrondissement
                    float t = Mathf.Round(this.transform.position.x * 100f) / 100f;



                    if (h == t) //if touch position = Beat position
                    {
                        float t2 = this.transform.localPosition.z; //local position of the beat

                        if ((t2 < 0.488) || (t2 > 0.501)) //if its not in the perfect zone
                        {
                            GameManager.Instance.NoteHit();

                        }
                        else
                        {
                            GameManager.Instance.NotePerfectHit(); //its in the perfect zone
                        }
                        Destroy(gameObject);
                    }

                }
            }

        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Beat"))
        {
            CanBePressed = true;
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Beat"))
        {
            CanBePressed = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("Beat"))
        {
            CanBePressed = false;
            GameManager.Instance.NoteMiss();
            Destroy(gameObject);
            //here the Health thing code

        }

    }


}
