﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    int comboCount, heartsCount;
    public int combo;
    int comboNote = 10;

    int PerfectHitNote = 150;
    public int currentScore;
    public int NormalHitNote = 100;

    public GameObject Dog;

    public Text hitText, comboText, finalScoreText, perfectCountText, normalCountText, missCountText, CoinText, scoreUI, heartsCountText;

    int perfectHitCount;
    int normalHitCount;
    int missHitCount, coinScore;

    public GameObject Result, GameOverMenu, PauseMenu;

    public static GameManager Instance;

    float jumpForce = 300;

    Animator anim;
    Rigidbody rb;

    Scene scene;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        heartsCount = 5;

        scene = SceneManager.GetActiveScene(); //Get Actuve current scene

        GameOverMenu.SetActive(false);
        PauseMenu.SetActive(false);

        anim = Dog.GetComponent<Animator>();
        rb = Dog.GetComponent<Rigidbody>();


    }

    // Update is called once per frame
    void Update()
    {



    }


    public void NoteHit()
    {
        comboCount++;
        combo += comboNote;
        currentScore += NormalHitNote;
        currentScore = currentScore + combo;

        hitText.color = Color.blue;
        hitText.text = "NORMAL";
        comboText.text = comboCount.ToString();
        normalHitCount++;

        rb.AddForce(0, jumpForce, 0);
        anim.SetTrigger("jump");

        scoreUI.text = currentScore.ToString();


    }

    public void NoteMiss()
    {
        comboCount = 0;
        combo = 0;

        hitText.color = Color.red;
        hitText.text = "MISSED";
        missHitCount++;

        heartsCount--;
        heartsCountText.text = heartsCount.ToString();

        if (heartsCount == 0)
        {
            GameOverMenu.SetActive(true);
            Time.timeScale = 0f; //Freezes the game time
            AudioListener.pause = true; //Pauses all Audios

        }
        comboText.text = " ";

    }

    //Perfect Hit Score count
    public void NotePerfectHit()
    {
        comboCount++;
        combo += comboNote;
        currentScore += PerfectHitNote;
        currentScore = currentScore + combo;
        perfectHitCount++;

        scoreUI.text = currentScore.ToString();

        hitText.text = "PERFECT";
        hitText.color = Color.magenta;
        comboText.text = comboCount.ToString();

    }

    //Result Scene scores
    public void ResultScreen()
    {
        finalScoreText.text = currentScore.ToString();
        perfectCountText.text = perfectHitCount.ToString();
        normalCountText.text = normalHitCount.ToString();
        missCountText.text = missHitCount.ToString();

        coinScore = currentScore / 5;
        CoinText.text = coinScore.ToString();
    }

    public void Pause()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0f; //Freezes the game time
        AudioListener.pause = true;

    }

    public void Resume()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1f; //unfreezes the game time
        AudioListener.pause = false;

    }

    public void Replay()
    {

        SceneManager.LoadScene(scene.name); //Load the current scene again
        Time.timeScale = 1f;
    }


}
