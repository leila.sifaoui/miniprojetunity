﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public GameObject OptionMenu;
    public AudioSource audiosource;
    public Slider slider;
    public Text TxtMusic;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenOptions()
    {
        if(OptionMenu != null){
            OptionMenu.SetActive(true);
        }
    }

    public void CloseOptions()
    {
        if(OptionMenu != null){
            OptionMenu.SetActive(false);
        }
    }


    public void SliderChanger()
    {
        audiosource.volume = slider.value;
        TxtMusic.text = "Music " + (audiosource.volume * 100).ToString("00") + "%";

    }
}
