﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SwitchScenes : MonoBehaviour
{
   public void Levels() {
       SceneManager.LoadScene(1);
   }

   public void HomePage() {
       SceneManager.LoadScene(0);
   }

   public void Level0() {
       SceneManager.LoadScene(2);
   }

   public void Missions() {
       SceneManager.LoadScene(3);
   }

   public void ProfilePage() {
       SceneManager.LoadScene(4);
   }
    
    public void Ranking() {
       SceneManager.LoadScene(5);
   }

   public void Shop() {
       SceneManager.LoadScene(6);
   }

    public void Cards() {
       SceneManager.LoadScene(7);
   }
}
